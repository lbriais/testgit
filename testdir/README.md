#Process description#
Maintenance-legacy contains maintenance tasks running on MUCVEP202.
At this time, it only contains SQL scheduled jobs: 
- for MUCVEP202:
	- Statistics - Maintenance - Stat DBs Maintenance.sql : execute maintenance plans
	- Statistics - Maintenance - Backup SEP_eCommerce_PNRs_History and zip.sql : executes master..sp_ar_backup2gz for dbname = 'SEP_eCommerce_PNRs_History',...
	- Statistics - Maintenance - CleanUp Archives Files.sql : calls G:\tools\Files_Cleanup\Cleanup_ArchivesZip.cmd and G:\tools\Files_Cleanup\Cleanup_Logs.cmd
	- Statistics - Maintenance - Copy files to local.sql : calls G:\tools\Files_Copy\CopyFiles_StatisticsZip.cmd, CopyFiles_WLSapplogsZip.cmd and CopyFiles_TracesZip.cmd
	- Statistics - Maintenance - FlexPricer Monthly FlexRoutes Purge.sql : cleanup SEP_eCommerce_FlexPricer.dbo.FlexRoutes
	- Statistics - Maintenance - MeRCI Archive.sql : fills SEP_eCommerce_Temp.dbo.TP_MeRCI_ACQ_Archive 
	- Statistics - Maintenance - save job history for capacity planning.sql : fills CapacityAnalysis.dbo.SQLjobHistory (monitoring sql jobs)
	- Statistics - Maintenance - Tools_Statistics_Billing_Environment_BackUp.sql : calls F:\Tools_Backup\Xcopy_Zip_Statistics_Billing_Environment.bat
-for MUCETLP101:
	- Statistics - Maintenance - Archives files cleanup.sql : Calls G:\tools\Files_Cleanup\Cleanup_ArchivesZip.cmd and  Cleanup_Logs.cmd
	- Statistics - Maintenance - Copy files to local.sql : calls CALL G:\tools\Files_Copy\CopyFiles_WLSapplogsZip.cmd
	- Statistics - Maintenance - stats db backup.sql : calls F:\DB_Backup\Zip_db_Backups.bat


#Input & Suppliers#

- SEP ecommerce datamarts
	+ BRM team


#Output & Customers#

- SEP ecommerce datamarts
	+ BRM team
- maintenance task execution logs
	+ BRM team
- notification of failure (for some tasks only)
	+ BRM team